package kladionicaii.kladionica.restClasses;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kladionicaii.kladionica.pojoClasses.Utakmica;
import kladionicaii.kladionica.serviceClasses.UtakmicaService;

@RestController
@RequestMapping("/api")
public class UtakmicaRestController {
	
	@Autowired
	private UtakmicaService utakmicaService;

	@GetMapping("/utakmica")
	public List<Utakmica> findAll() {
		return utakmicaService.findAll();
	}
	
	@GetMapping("/utakmica/{utakmicaid}")
	public Utakmica getUtakmica(@PathVariable Integer utakmicaid) {
		Utakmica utakmica = utakmicaService.findById(utakmicaid);
		return utakmica;
	}
	
	@PostMapping("/utakmica/save")
	public Utakmica addUtakmica(@RequestBody Utakmica utakmica) {
		utakmicaService.save(utakmica);
		return utakmica;
	}
	
	@PutMapping("/utakmica/update")
	public Utakmica updateUtakmica(@RequestBody Utakmica utakmica) {
		Utakmica updateUtakmica = utakmicaService.update(utakmica);
		return updateUtakmica;
	}
	
	@DeleteMapping("/utakmica/delete/{utakmicaid}")
	public void deleteUtakmica(@PathVariable Integer utakmicaid) {
		utakmicaService.deleteById(utakmicaid);
	}
	
	@DeleteMapping("/utakmica/delete")
	public void deleteUtakmicaObject(@RequestBody Utakmica utakmica) {
		utakmicaService.deleteByObject(utakmica);
	}
	
}
