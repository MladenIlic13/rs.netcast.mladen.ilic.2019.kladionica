package kladionicaii.kladionica.restClasses;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kladionicaii.kladionica.pojoClasses.Tiket;
import kladionicaii.kladionica.serviceClasses.TiketService;

@RestController
@RequestMapping("/api")
public class TiketRestController {
	
	@Autowired
	private TiketService tiketService;

	@GetMapping("/tiket")
	public List<Tiket> findAll() {
		return tiketService.findAll();
	}
	
	@GetMapping("/tiket/{tiketid}")
	public Tiket getTiket(@PathVariable Integer tiketid) {
		Tiket tiket = tiketService.findById(tiketid);
		return tiket;
	}
	
	@PostMapping("/tiket/save")
	public Tiket addTiket(@RequestBody Tiket tiket) {
		tiketService.save(tiket);
		return tiket;
	}
	
	@PutMapping("/tiket/update")
	public Tiket updateTiket(@RequestBody Tiket tiket) {
		Tiket updateTiket = tiketService.update(tiket);
		return updateTiket;
	}
	
	@DeleteMapping("/tiket/delete/{tiketid}")
	public void deleteTiket(@PathVariable Integer tiketid) {
		tiketService.deleteById(tiketid);
	}
	
	@DeleteMapping("/tiket/delete")
	public void deleteTiketObject(@RequestBody Tiket tiket) {
		tiketService.deleteByObject(tiket);
	}
	
}
