package kladionicaii.kladionica.restClasses;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kladionicaii.kladionica.pojoClasses.UtakmicaTiket;
import kladionicaii.kladionica.serviceClasses.UtakmicaTiketService;

@RestController
@RequestMapping("/api")
public class UtakmicaTiketRestController {
	
	@Autowired
	private UtakmicaTiketService utakmicaTiketService;

	@GetMapping("/utakmicatiket")
	public List<UtakmicaTiket> findAll() {
		return utakmicaTiketService.findAll();
	}
	
	@GetMapping("/utakmicatiket/{utakmicatiketid}")
	public UtakmicaTiket getUtakmicaTiket(@PathVariable Integer utakmicatiketid) {
		UtakmicaTiket utakmicaTiket = utakmicaTiketService.findById(utakmicatiketid);
		return utakmicaTiket;
	}
	
	@PostMapping("/utakmicatiket/save")
	public UtakmicaTiket addUtakmicaTiket(@RequestBody UtakmicaTiket utakmicaTiket) {
		utakmicaTiketService.save(utakmicaTiket);
		return utakmicaTiket;
	}
	
	@PutMapping("/utakmicatiket/update")
	public UtakmicaTiket updateUtakmicaTiket(@RequestBody UtakmicaTiket utakmicaTiket) {
		UtakmicaTiket updateUtakmicaTiket = utakmicaTiketService.update(utakmicaTiket);
		return updateUtakmicaTiket;
	}
	
	@DeleteMapping("/utakmicatiket/delete/{utakmicatiketid}")
	public void deleteUtakmicaTiket(@PathVariable Integer utakmicatiketid) {
		utakmicaTiketService.deleteById(utakmicatiketid);
	}
	
	@DeleteMapping("/utakmicatiket/delete")
	public void deleteUtakmicaTiketObject(@RequestBody UtakmicaTiket utakmicaTiket) {
		utakmicaTiketService.deleteByObject(utakmicaTiket);
	}
	
}
