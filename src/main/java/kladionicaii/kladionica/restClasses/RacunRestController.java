package kladionicaii.kladionica.restClasses;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kladionicaii.kladionica.pojoClasses.Racun;
import kladionicaii.kladionica.serviceClasses.RacunService;

@RestController
@RequestMapping("/api")
public class RacunRestController {
	
	@Autowired
	private RacunService racunService;

	@GetMapping("/racun")
	public List<Racun> findAll() {
		return racunService.findAll();
	}
	
	@GetMapping("/racun/{racunid}")
	public Racun getRacun(@PathVariable Integer racunid) {
		Racun racun = racunService.findById(racunid);
		return racun;
	}
	
	@PostMapping("/racun/save")
	public Racun addRacun(@RequestBody Racun racun) {
		racunService.save(racun);
		return racun;
	}
	
	@PutMapping("/racun/update")
	public Racun updateRacun(@RequestBody Racun racun) {
		Racun updateRacun = racunService.update(racun);
		return updateRacun;
	}
	
	@DeleteMapping("/racun/delete/{racunid}")
	public void deleteRacun(@PathVariable Integer racunid) {
		racunService.deleteById(racunid);
	}
	
	@DeleteMapping("/racun/delete")
	public void deleteRacunObject(@RequestBody Racun racun) {
		racunService.deleteByObject(racun);
	}
	
}
