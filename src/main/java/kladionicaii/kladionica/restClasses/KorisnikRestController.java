package kladionicaii.kladionica.restClasses;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kladionicaii.kladionica.pojoClasses.Korisnik;
import kladionicaii.kladionica.serviceClasses.KorisnikService;

@RestController
@RequestMapping("/api")
public class KorisnikRestController {
	
	@Autowired
	private KorisnikService korisnikService;

	@GetMapping("/korisnik")
	public List<Korisnik> findAll() {
		return korisnikService.findAll();
	}
	
	@GetMapping("/korisnik/{korisnikid}")
	public Korisnik getKorisnik(@PathVariable Integer korisnikid) {
		Korisnik korisnik = korisnikService.findById(korisnikid);
		return korisnik;
	}
	
	@PostMapping("/korisnik/save")
	public Korisnik addKorisnik(@RequestBody Korisnik korisnik) {
		korisnikService.save(korisnik);
		return korisnik;
	}
	
	@PutMapping("/korisnik/update")
	public Korisnik updateKorisnik(@RequestBody Korisnik korisnik) {
		Korisnik updateKorisnik = korisnikService.update(korisnik);
		return updateKorisnik;
	}
	
	@DeleteMapping("/korisnik/delete/{korisnikid}")
	public void deleteKorisnik(@PathVariable Integer korisnikid) {
		korisnikService.deleteById(korisnikid);
	}
	
	@DeleteMapping("/korisnik/delete")
	public void deleteKorisnikObject(@RequestBody Korisnik korisnik) {
		korisnikService.deleteByObject(korisnik);
	}
	
}
