
package kladionicaii.kladionica.servicezDTOImplementationClasses;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kladionicaii.kladionica.daoClasses.RacunRepository;
import kladionicaii.kladionica.pojoClasses.Korisnik;
import kladionicaii.kladionica.pojoClasses.Racun;
import kladionicaii.kladionica.pojoDTOClasses.NovcaneTransakcijeInputDTO;
import kladionicaii.kladionica.pojoDTOClasses.NovcaneTransakcijeOutputDTO;
import kladionicaii.kladionica.servicezDTOClasses.NovcaneTransakcijeServiceDTO;

@Service
public class NovcaneTransakcijeServiceDTOImpl implements NovcaneTransakcijeServiceDTO {

	private RacunRepository racunRepository;
	
	@Autowired
	public NovcaneTransakcijeServiceDTOImpl(RacunRepository racunRepository) {
		this.racunRepository = racunRepository;
	}

	@Override
	public List<NovcaneTransakcijeOutputDTO> pregled(NovcaneTransakcijeInputDTO novcaneTransakcijeInputDTO) {
		
		Korisnik korisnik = novcaneTransakcijeInputDTO.getKorisnik();
		LocalDateTime odVremena = novcaneTransakcijeInputDTO.getOdvremena();
		LocalDateTime doVremena = novcaneTransakcijeInputDTO.getDovremena();
		
		List<Racun> listRacun = racunRepository.findByKorisnikAndVremeBetween(korisnik, odVremena, doVremena);
		List<NovcaneTransakcijeOutputDTO> listDTO = new ArrayList<>();
		
		for(Racun racun : listRacun) {
			NovcaneTransakcijeOutputDTO novcaneTransakcijeOutputDTO = new NovcaneTransakcijeOutputDTO();
			novcaneTransakcijeOutputDTO.setIdTransakcije(racun.getId());
			novcaneTransakcijeOutputDTO.setIznos(racun.getIznos());
			novcaneTransakcijeOutputDTO.setVreme(racun.getVreme());
			novcaneTransakcijeOutputDTO.setTipSvrha(racun.getSvrha());
			listDTO.add(novcaneTransakcijeOutputDTO);
		}
		
		return listDTO;
	}

	@Override
	public NovcaneTransakcijeInputDTO customInput() {
		Korisnik korisnik = new Korisnik();
		korisnik.setId(1);
		korisnik.setEmail("mladenilic@gmail.com");
		korisnik.setNaziv("mladen");
		LocalDateTime time1 = LocalDateTime.now();
		LocalDateTime time2 = LocalDateTime.now();
		NovcaneTransakcijeInputDTO novcaneTransakcijeInputDTO = new NovcaneTransakcijeInputDTO();
		novcaneTransakcijeInputDTO.setKorisnik(korisnik);
		novcaneTransakcijeInputDTO.setOdvremena(time1);
		novcaneTransakcijeInputDTO.setDovremena(time2);
		return novcaneTransakcijeInputDTO;
	}

	

}
