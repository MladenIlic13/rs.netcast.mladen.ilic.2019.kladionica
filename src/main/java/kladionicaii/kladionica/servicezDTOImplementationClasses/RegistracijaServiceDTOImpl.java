
package kladionicaii.kladionica.servicezDTOImplementationClasses;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kladionicaii.kladionica.daoClasses.KorisnikRepository;
import kladionicaii.kladionica.daoClasses.RacunRepository;
import kladionicaii.kladionica.pojoClasses.Korisnik;
import kladionicaii.kladionica.pojoClasses.Racun;
import kladionicaii.kladionica.pojoDTOClasses.RegistracijaDTO;
import kladionicaii.kladionica.servicezDTOClasses.RegistracijaServiceDTO;

@Service
public class RegistracijaServiceDTOImpl implements RegistracijaServiceDTO {

	private KorisnikRepository korisnikRepository;
	private RacunRepository racunRepository;
	
	@Autowired
	public RegistracijaServiceDTOImpl(KorisnikRepository korisnikRepository,
			RacunRepository racunRepository) {
		this.korisnikRepository = korisnikRepository;
		this.racunRepository = racunRepository;
	}
	
	@Override
	@Transactional
	public RegistracijaDTO save(RegistracijaDTO registracija) {
		Korisnik korisnik = new Korisnik(registracija.getNaziv(), registracija.getEmail());
		korisnikRepository.save(korisnik);
		Racun racun = new Racun("registracija", BigDecimal.valueOf(5), LocalDateTime.now(), korisnik);
		racunRepository.save(racun);
		return registracija;
	}

}
