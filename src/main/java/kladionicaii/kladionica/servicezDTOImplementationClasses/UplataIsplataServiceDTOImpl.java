
package kladionicaii.kladionica.servicezDTOImplementationClasses;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kladionicaii.kladionica.daoClasses.RacunRepository;
import kladionicaii.kladionica.pojoClasses.Racun;
import kladionicaii.kladionica.pojoDTOClasses.UplataIsplataDTO;
import kladionicaii.kladionica.servicezDTOClasses.UplataIsplataServiceDTO;

@Service
public class UplataIsplataServiceDTOImpl implements UplataIsplataServiceDTO {

	private RacunRepository racunRepository;
	
	@Autowired
	public UplataIsplataServiceDTOImpl(RacunRepository racunRepository) {
		this.racunRepository = racunRepository;
	}

	@Override
	@Transactional
	public UplataIsplataDTO uplata(UplataIsplataDTO uplata) {
		BigDecimal bigDecimal = uplata.getIznos();
		Racun racunKorisnik = 
				new Racun("uplata", bigDecimal, LocalDateTime.now(), uplata.getKorisnik());
		racunRepository.save(racunKorisnik);
		return uplata;
	}

	@Override
	@Transactional
	public UplataIsplataDTO isplata(UplataIsplataDTO isplata) {
		BigDecimal bigDecimal = isplata.getIznos();
		Racun racunKorisnik = 
				new Racun("isplata", bigDecimal, LocalDateTime.now(), isplata.getKorisnik());
		racunRepository.save(racunKorisnik);
		return isplata;
	}

}
