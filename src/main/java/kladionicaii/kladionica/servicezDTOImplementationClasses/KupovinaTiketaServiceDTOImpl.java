
package kladionicaii.kladionica.servicezDTOImplementationClasses;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kladionicaii.kladionica.daoClasses.KorisnikRepository;
import kladionicaii.kladionica.daoClasses.RacunRepository;
import kladionicaii.kladionica.daoClasses.TiketRepository;
import kladionicaii.kladionica.daoClasses.UtakmicaRepository;
import kladionicaii.kladionica.daoClasses.UtakmicaTiketRepository;
import kladionicaii.kladionica.pojoClasses.Korisnik;
import kladionicaii.kladionica.pojoClasses.Racun;
import kladionicaii.kladionica.pojoClasses.Tiket;
import kladionicaii.kladionica.pojoClasses.Utakmica;
import kladionicaii.kladionica.pojoClasses.UtakmicaTiket;
import kladionicaii.kladionica.pojoDTOClasses.KupovinaTiketaDTO;
import kladionicaii.kladionica.servicezDTOClasses.KupovinaTiketaServiceDTO;

@Service
public class KupovinaTiketaServiceDTOImpl implements KupovinaTiketaServiceDTO {

	private UtakmicaTiketRepository utakmicaTiketRepository;
	
	private RacunRepository racunRepository;
	
	private TiketRepository tiketRepository;
	
	private KorisnikRepository korisnikRepository;
	
	private UtakmicaRepository utakmicaRepository;
	
	@Autowired
	public KupovinaTiketaServiceDTOImpl(UtakmicaTiketRepository utakmicaTiketRepository,
			RacunRepository racunRepository, TiketRepository tiketRepository, KorisnikRepository korisnikRepository,
			UtakmicaRepository utakmicaRepository) {
		this.utakmicaTiketRepository = utakmicaTiketRepository;
		this.racunRepository = racunRepository;
		this.tiketRepository = tiketRepository;
		this.korisnikRepository = korisnikRepository;
		this.utakmicaRepository = utakmicaRepository;
	}

	@Override
	@Transactional
	public KupovinaTiketaDTO kupovina(KupovinaTiketaDTO kupovinaTiketa) {
		
		// izvadi podatke
		Integer korisnikid = kupovinaTiketa.getKorisnikId();
		BigDecimal bigDecimal = kupovinaTiketa.getIznos();
		Integer[] arrayUtakmica = kupovinaTiketa.getArrayUtakmicaId();
		Integer[] arrayPredvidjanja = kupovinaTiketa.getArrayPredvidjanje();

		Optional<Korisnik> result = korisnikRepository.findById(korisnikid);
		Korisnik korisnik = null;
		if (result.isPresent()) {
			korisnik = result.get();
		} else {
			System.out.println("nema resenja");
		}

		// prepakuje array u arraylistu
		ArrayList<Utakmica> arrayListUtakmica = new ArrayList<>();
		for (Integer utakmicaid : arrayUtakmica) {
			Optional<Utakmica> result2 = utakmicaRepository.findById(utakmicaid);
			Utakmica utakmica = null;
			if (result2.isPresent()) {
				utakmica = result2.get();
				arrayListUtakmica.add(utakmica);
			} else {
				System.out.println("nema resenja");
			}
		}

		// kreiraj tiket
		Tiket tiket = new Tiket(bigDecimal, korisnik);
		tiketRepository.save(tiket);
		
		// kreiraj racun
		Racun racun = new Racun("kupovina", bigDecimal, LocalDateTime.now(), korisnik);
		racunRepository.save(racun);
		
		// kreiraj utakmica-tiket objekte
		for (int i=0; i<arrayListUtakmica.size(); i++) {
			Utakmica utakmica = arrayListUtakmica.get(i);
			Integer predvidjanje = arrayPredvidjanja[i];
			UtakmicaTiket utakmicaTiket = new UtakmicaTiket(predvidjanje, tiket, utakmica);
			utakmicaTiketRepository.save(utakmicaTiket);
		}

		return kupovinaTiketa;
	}

	

	// test
	@Override
	public KupovinaTiketaDTO customOutput() {
		
		Integer korisnikId = 1;
		BigDecimal bigDecimal = BigDecimal.valueOf(333);
		
		List<UtakmicaTiket> lista = utakmicaTiketRepository.findAll();
		
		Integer[] arrayIdUtakmica = new Integer[3];
		arrayIdUtakmica[0] = lista.get(0).getUtakmica().getId();
		arrayIdUtakmica[1] = lista.get(1).getUtakmica().getId();
		arrayIdUtakmica[2] = lista.get(2).getUtakmica().getId();
		
		Integer[] arrayPredvidjanja = new Integer[3];
		arrayPredvidjanja[0] = lista.get(0).getPredvidjanje();
		arrayPredvidjanja[1] = lista.get(1).getPredvidjanje();
		arrayPredvidjanja[2] = lista.get(2).getPredvidjanje();
		
		KupovinaTiketaDTO kupovina = new KupovinaTiketaDTO(korisnikId, bigDecimal,
				arrayIdUtakmica, arrayPredvidjanja);
		
		return kupovina;
	}

}
