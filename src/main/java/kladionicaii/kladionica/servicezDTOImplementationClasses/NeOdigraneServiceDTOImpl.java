
package kladionicaii.kladionica.servicezDTOImplementationClasses;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kladionicaii.kladionica.daoClasses.UtakmicaRepository;
import kladionicaii.kladionica.pojoClasses.Utakmica;
import kladionicaii.kladionica.pojoDTOClasses.NeOdigraneDTO;
import kladionicaii.kladionica.servicezDTOClasses.NeOdigraneServiceDTO;

@Service
public class NeOdigraneServiceDTOImpl implements NeOdigraneServiceDTO {

	private UtakmicaRepository utakmicaRepository;
	
	@Autowired
	public NeOdigraneServiceDTOImpl(UtakmicaRepository utakmicaRepository) {
		this.utakmicaRepository = utakmicaRepository;
	}
	
	@Override
	public List<NeOdigraneDTO> neOdigrane() {
		List<Utakmica> utakmice = this.utakmicaRepository.findAll();
		List<NeOdigraneDTO> neOdigrane = new ArrayList<>();
		for (Utakmica utakmica : utakmice) {
			if (utakmica.getFlag()==null) {
				NeOdigraneDTO neOdigraneDTO = new NeOdigraneDTO();
				neOdigraneDTO.setId(utakmica.getId());
				neOdigraneDTO.setNaziv(utakmica.getNaziv());
				neOdigraneDTO.setDate(utakmica.getDate());
				neOdigrane.add(neOdigraneDTO);
			}
		}
		return neOdigrane;
	}
	
}
