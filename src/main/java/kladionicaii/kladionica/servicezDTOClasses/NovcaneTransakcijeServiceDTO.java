
package kladionicaii.kladionica.servicezDTOClasses;

import java.util.List;

import kladionicaii.kladionica.pojoDTOClasses.NovcaneTransakcijeInputDTO;
import kladionicaii.kladionica.pojoDTOClasses.NovcaneTransakcijeOutputDTO;

public interface NovcaneTransakcijeServiceDTO {
	
	public List<NovcaneTransakcijeOutputDTO> pregled(NovcaneTransakcijeInputDTO novcaneTransakcijeInputDTO);

	public NovcaneTransakcijeInputDTO customInput();
	
}
