
package kladionicaii.kladionica.servicezDTOClasses;

import java.util.List;

import kladionicaii.kladionica.pojoDTOClasses.NeOdigraneDTO;

public interface NeOdigraneServiceDTO {
	
	// prosledjuje od baze ka korisniku
	public List<NeOdigraneDTO> neOdigrane();
	  
}
