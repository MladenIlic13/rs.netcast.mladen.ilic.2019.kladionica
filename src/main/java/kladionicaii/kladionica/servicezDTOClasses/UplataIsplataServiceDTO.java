
package kladionicaii.kladionica.servicezDTOClasses;

import kladionicaii.kladionica.pojoDTOClasses.UplataIsplataDTO;

public interface UplataIsplataServiceDTO {
	
	public UplataIsplataDTO uplata(UplataIsplataDTO uplataIsplata);
	
	public UplataIsplataDTO isplata(UplataIsplataDTO uplataIsplata);
	  
}
