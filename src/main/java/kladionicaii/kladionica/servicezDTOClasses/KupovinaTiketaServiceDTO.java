
package kladionicaii.kladionica.servicezDTOClasses;

import kladionicaii.kladionica.pojoDTOClasses.KupovinaTiketaDTO;

public interface KupovinaTiketaServiceDTO {
	
	public KupovinaTiketaDTO kupovina(KupovinaTiketaDTO kupovinaTiketa);
	
	public KupovinaTiketaDTO customOutput();

}
