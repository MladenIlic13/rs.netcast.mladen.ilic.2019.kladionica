
package kladionicaii.kladionica.servicezDTOClasses;

import kladionicaii.kladionica.pojoDTOClasses.RegistracijaDTO;

public interface RegistracijaServiceDTO {
	
	public RegistracijaDTO save(RegistracijaDTO registracija);
	  
}
