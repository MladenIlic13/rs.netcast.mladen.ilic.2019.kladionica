package kladionicaii.kladionica.serviceImplementationClasses;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kladionicaii.kladionica.daoClasses.KorisnikRepository;
import kladionicaii.kladionica.exceptions.KorisnikNotFoundException;
import kladionicaii.kladionica.pojoClasses.Korisnik;
import kladionicaii.kladionica.serviceClasses.KorisnikService;

@Service
public class KorisnikServiceImpl implements KorisnikService {

	private KorisnikRepository korisnikRepository;
	
	@Autowired
	public KorisnikServiceImpl(KorisnikRepository korisnikRepository) {
		this.korisnikRepository = korisnikRepository;
	}

	@Override
	public List<Korisnik> findAll() {
		return korisnikRepository.findAll();
	}

	@Override
	public Korisnik findById(Integer id) {
		Optional<Korisnik> result = korisnikRepository.findById(id);
		
		// exception handling
		Korisnik korisnik = null;
		if (result.isPresent()) {
			korisnik = result.get();
		} else {
			// pass some data if needed id in this case
			throw new KorisnikNotFoundException(id);
		}
		return korisnik;
	}
	
	@Override
	public Korisnik save(Korisnik korisnik) {
		korisnikRepository.save(korisnik);
		return korisnik;
	}
	
	@Override
	public Korisnik update(Korisnik korisnik) {
		Integer tempid = korisnik.getId();
		Optional<Korisnik> result = korisnikRepository.findById(tempid);
		Korisnik updateKorisnik = null;
		if (result.isPresent()) {
			updateKorisnik = result.get();
			updateKorisnik.setNaziv(korisnik.getNaziv());
			updateKorisnik.setEmail(korisnik.getEmail());
			korisnikRepository.save(updateKorisnik);
		} else {
			System.out.println("nema resenja");
		}
		return updateKorisnik;
	}
	
	@Override
	public void deleteById(Integer id) {
		korisnikRepository.deleteById(id);
	}

	@Override
	public void deleteByObject(Korisnik korisnik) {
		korisnikRepository.delete(korisnik);
		
	}

}
