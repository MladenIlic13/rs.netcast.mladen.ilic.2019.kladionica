package kladionicaii.kladionica.serviceImplementationClasses;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kladionicaii.kladionica.daoClasses.RacunRepository;
import kladionicaii.kladionica.pojoClasses.Racun;
import kladionicaii.kladionica.serviceClasses.RacunService;

@Service
public class RacunServiceImpl implements RacunService {

	private RacunRepository racunRepository;
	
	@Autowired
	public RacunServiceImpl(RacunRepository racunRepository) {
		this.racunRepository = racunRepository;
	}
	
	@Override
	public List<Racun> findAll() {
		return racunRepository.findAll();
	}

	@Override
	public Racun findById(Integer id) {
		Optional<Racun> result = racunRepository.findById(id);
		Racun racun = null;
		if (result.isPresent()) {
			racun = result.get();
		} else {
			System.out.println("nema resenja");
		}
		return racun;
	}
	
	@Override
	public Racun save(Racun racun) {
		racunRepository.save(racun);
		return racun;
	}

	@Override
	public Racun update(Racun racun) {
		Integer tempid = racun.getId();
		Optional<Racun> result = racunRepository.findById(tempid);
		Racun updateRacun = null;
		if (result.isPresent()) {
			updateRacun = result.get();
			updateRacun.setSvrha(racun.getSvrha());
			updateRacun.setIznos(racun.getIznos());
			updateRacun.setVreme(racun.getVreme());
			updateRacun.setKorisnik(racun.getKorisnik());
			racunRepository.save(updateRacun);
		} else {
			System.out.println("nema resenja");
		}
		return updateRacun;
	}
	
	@Override
	public void deleteById(Integer id) {
		racunRepository.deleteById(id);
	}

	@Override
	public void deleteByObject(Racun racun) {
		racunRepository.delete(racun);
	}

}
