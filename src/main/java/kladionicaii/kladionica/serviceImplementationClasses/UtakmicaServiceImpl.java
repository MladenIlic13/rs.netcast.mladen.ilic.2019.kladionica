package kladionicaii.kladionica.serviceImplementationClasses;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kladionicaii.kladionica.daoClasses.UtakmicaRepository;
import kladionicaii.kladionica.pojoClasses.Utakmica;
import kladionicaii.kladionica.serviceClasses.UtakmicaService;

@Service
public class UtakmicaServiceImpl implements UtakmicaService {

	private UtakmicaRepository utakmicaRepository;
	
	@Autowired
	public UtakmicaServiceImpl(UtakmicaRepository utakmicaRepository) {
		this.utakmicaRepository = utakmicaRepository;
	}

	@Override
	public List<Utakmica> findAll() {
		return utakmicaRepository.findAll();
	}

	@Override
	public Utakmica findById(Integer id) {
		Optional<Utakmica> result = utakmicaRepository.findById(id);
		Utakmica utakmica = null;
		if (result.isPresent()) {
			utakmica = result.get();
		} else {
			System.out.println("nema resenja");
		}
		return utakmica;
	}
	
	@Override
	public Utakmica save(Utakmica utakmica) {
		utakmicaRepository.save(utakmica);
		return utakmica;
	}

	@Override
	public Utakmica update(Utakmica utakmica) {
		Integer tempid = utakmica.getId();
		Optional<Utakmica> result = utakmicaRepository.findById(tempid);
		Utakmica updateUtakmica = null;
		if (result.isPresent()) {
			updateUtakmica = result.get();
			updateUtakmica.setNaziv(utakmica.getNaziv());
			updateUtakmica.setFlag(utakmica.getFlag());
			updateUtakmica.setDate(utakmica.getDate());
			updateUtakmica.setOperater(utakmica.getOperater());
			utakmicaRepository.save(updateUtakmica);
		} else {
			System.out.println("nema resenja");
		}
		return updateUtakmica;
	}
	
	@Override
	public void deleteById(Integer id) {
		utakmicaRepository.deleteById(id);
	}

	@Override
	public void deleteByObject(Utakmica utakmica) {
		utakmicaRepository.delete(utakmica);
	}

}
