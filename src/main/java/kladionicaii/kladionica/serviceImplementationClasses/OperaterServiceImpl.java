package kladionicaii.kladionica.serviceImplementationClasses;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kladionicaii.kladionica.daoClasses.OperaterRepository;
import kladionicaii.kladionica.pojoClasses.Operater;
import kladionicaii.kladionica.serviceClasses.OperaterService;

@Service
public class OperaterServiceImpl implements OperaterService {

	// can be single field autowired
	private OperaterRepository operaterRepository;
	
	// use constructor autowireing for more complex constructors
	@Autowired
	public OperaterServiceImpl(OperaterRepository operaterRepository) {
		this.operaterRepository = operaterRepository;
	}

	@Override
//	@Transactional put transactional if needed
	public List<Operater> findAll() {
		return operaterRepository.findAll();
	}

	@Override
	public Operater findById(Integer id) {
		Optional<Operater> result = operaterRepository.findById(id);
		Operater operater = null;
		if (result.isPresent()) {
			operater = result.get();
		} else {
			System.out.println("nema resenja");
		}
		return operater;
	}
	
	@Override
	public Operater save(Operater operater) {
		operaterRepository.save(operater);
		// alternativno
//		operaterRepository.saveAndFlush(operater);
		return operater;
	}

	@Override
	public Operater update1(Integer id, String name) {
		Optional<Operater> result = operaterRepository.findById(id);
		Operater updateOperater = null;
		if (result.isPresent()) {
			updateOperater = result.get();
			updateOperater.setNaziv(name);
			operaterRepository.save(updateOperater);
		} else {
			System.out.println("nema resenja");
		}
		return updateOperater;
	}
	
	@Override
	public Operater update2(Operater operater) {
		Integer tempid = operater.getId();
		Optional<Operater> result = operaterRepository.findById(tempid);
		Operater updateOperater = null;
		if (result.isPresent()) {
			updateOperater = result.get();
			updateOperater.setNaziv(operater.getNaziv());
			operaterRepository.save(updateOperater);
		} else {
			System.out.println("nema resenja");
		}
		return updateOperater;
	}
	
	@Override
	public void deleteById(Integer id) {
		operaterRepository.deleteById(id);
	}

	@Override
	public void deleteByObject(Operater operater) {
		operaterRepository.delete(operater);
		
	}

}
