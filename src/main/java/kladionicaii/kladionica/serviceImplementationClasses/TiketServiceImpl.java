package kladionicaii.kladionica.serviceImplementationClasses;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kladionicaii.kladionica.daoClasses.TiketRepository;
import kladionicaii.kladionica.pojoClasses.Tiket;
import kladionicaii.kladionica.serviceClasses.TiketService;

@Service
public class TiketServiceImpl implements TiketService {

	private TiketRepository tiketRepository;
	
	@Autowired
	public TiketServiceImpl(TiketRepository tiketRepository) {
		this.tiketRepository = tiketRepository;
	}

	@Override
	public List<Tiket> findAll() {
		return tiketRepository.findAll();
	}

	@Override
	public Tiket findById(Integer id) {
		Optional<Tiket> result = tiketRepository.findById(id);
		Tiket tiket = null;
		if (result.isPresent()) {
			tiket = result.get();
		} else {
			System.out.println("nema resenja");
		}
		return tiket;
	}
	
	@Override
	public Tiket save(Tiket tiket) {
		tiketRepository.save(tiket);
		return tiket;
	}

	@Override
	public Tiket update(Tiket tiket) {
		Integer tempid = tiket.getId();
		Optional<Tiket> result = tiketRepository.findById(tempid);
		Tiket updateTiket = null;
		if (result.isPresent()) {
			updateTiket = result.get();
			updateTiket.setUplaceno(tiket.getUplaceno());
			updateTiket.setKorisnik(tiket.getKorisnik());
			tiketRepository.save(updateTiket);
		} else {
			System.out.println("nema resenja");
		}
		return updateTiket;
	}
	
	@Override
	public void deleteById(Integer id) {
		tiketRepository.deleteById(id);
	}

	@Override
	public void deleteByObject(Tiket tiket) {
		tiketRepository.delete(tiket);
	}

}
