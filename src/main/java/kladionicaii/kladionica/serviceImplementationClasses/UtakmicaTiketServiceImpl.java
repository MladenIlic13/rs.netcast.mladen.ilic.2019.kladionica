package kladionicaii.kladionica.serviceImplementationClasses;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kladionicaii.kladionica.daoClasses.UtakmicaTiketRepository;
import kladionicaii.kladionica.pojoClasses.UtakmicaTiket;
import kladionicaii.kladionica.serviceClasses.UtakmicaTiketService;

@Service
public class UtakmicaTiketServiceImpl implements UtakmicaTiketService {

	private UtakmicaTiketRepository utakmicaTiketRepository;

	@Autowired
	public UtakmicaTiketServiceImpl(UtakmicaTiketRepository utakmicaTiketRepository) {
		this.utakmicaTiketRepository = utakmicaTiketRepository;
	}

	@Override
	public List<UtakmicaTiket> findAll() {
		return utakmicaTiketRepository.findAll();
	}

	@Override
	public UtakmicaTiket findById(Integer id) {
		Optional<UtakmicaTiket> result = utakmicaTiketRepository.findById(id);
		UtakmicaTiket utakmicaTiket = null;
		if (result.isPresent()) {
			utakmicaTiket = result.get();
		} else {
			System.out.println("nema resenja");
		}
		return utakmicaTiket;
	}
	
	@Override
	public UtakmicaTiket save(UtakmicaTiket utakmicaTiket) {
		utakmicaTiketRepository.save(utakmicaTiket);
		return utakmicaTiket;
	}

	@Override
	public UtakmicaTiket update(UtakmicaTiket utakmicaTiket) {
		Integer tempid = utakmicaTiket.getId();
		Optional<UtakmicaTiket> result = utakmicaTiketRepository.findById(tempid);
		UtakmicaTiket updateUtakmicaTiket = null;
		if (result.isPresent()) {
			updateUtakmicaTiket = result.get();
			updateUtakmicaTiket.setPredvidjanje(utakmicaTiket.getPredvidjanje());
			updateUtakmicaTiket.setTiket(utakmicaTiket.getTiket());
			updateUtakmicaTiket.setUtakmica(utakmicaTiket.getUtakmica());
			utakmicaTiketRepository.save(updateUtakmicaTiket);
		} else {
			System.out.println("nema resenja");
		}
		return updateUtakmicaTiket;
	}
	
	@Override
	public void deleteById(Integer id) {
		utakmicaTiketRepository.deleteById(id);
	}

	@Override
	public void deleteByObject(UtakmicaTiket utakmicaTiket) {
		utakmicaTiketRepository.delete(utakmicaTiket);
	}

}
