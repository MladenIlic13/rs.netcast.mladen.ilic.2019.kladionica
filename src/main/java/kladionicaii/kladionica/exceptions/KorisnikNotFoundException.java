package kladionicaii.kladionica.exceptions;

public class KorisnikNotFoundException extends RuntimeException{

	// for serialization ?? not relevant for exceptions ??
	/**
	 * 
	 */
	private static final long serialVersionUID = -100630816716347475L;
	
	public KorisnikNotFoundException(Integer id) {
		// insert custom message in default message of runtime exception if needed 
		super("korisnik sa id-jem " + id + " nije pronadjen.");
	}
	
}
