package kladionicaii.kladionica.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionClass {
	
	// methods for handling specific exceptions (separete for every exception)
	
	// response body returns messsage (in postman or..)
	@ResponseBody
	// specify exception
	@ExceptionHandler(value=KorisnikNotFoundException.class)
	// specify response status for controller method
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String korisnikNotFoundException(KorisnikNotFoundException exception){
		return exception.getMessage();
	}

}
