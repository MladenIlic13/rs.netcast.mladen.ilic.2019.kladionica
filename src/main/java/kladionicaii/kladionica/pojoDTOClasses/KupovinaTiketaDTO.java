
package kladionicaii.kladionica.pojoDTOClasses;

import java.math.BigDecimal;
import java.util.Arrays;

public class KupovinaTiketaDTO {

	private Integer korisnikId;

	private BigDecimal iznos;

	private Integer[] arrayUtakmicaId;
	
	private Integer[] arrayPredvidjanje;
	
	public KupovinaTiketaDTO() {
	}

	public KupovinaTiketaDTO(Integer korisnikId, BigDecimal iznos, Integer[] arrayUtakmicaId,
			Integer[] arrayPredvidjanje) {
		this.korisnikId = korisnikId;
		this.iznos = iznos;
		this.arrayUtakmicaId = arrayUtakmicaId;
		this.arrayPredvidjanje = arrayPredvidjanje;
	}

	public Integer getKorisnikId() {
		return korisnikId;
	}

	public void setKorisnikId(Integer korisnikId) {
		this.korisnikId = korisnikId;
	}

	public BigDecimal getIznos() {
		return iznos;
	}

	public void setIznos(BigDecimal iznos) {
		this.iznos = iznos;
	}

	public Integer[] getArrayUtakmicaId() {
		return arrayUtakmicaId;
	}

	public void setArrayUtakmicaId(Integer[] arrayUtakmicaId) {
		this.arrayUtakmicaId = arrayUtakmicaId;
	}

	public Integer[] getArrayPredvidjanje() {
		return arrayPredvidjanje;
	}

	public void setArrayPredvidjanje(Integer[] arrayPredvidjanje) {
		this.arrayPredvidjanje = arrayPredvidjanje;
	}

	@Override
	public String toString() {
		return "KupovinaTiketaDTO [korisnikId=" + korisnikId + ", iznos=" + iznos + ", arrayUtakmicaId="
				+ Arrays.toString(arrayUtakmicaId) + ", arrayPredvidjanje=" + Arrays.toString(arrayPredvidjanje) + "]";
	}

}
