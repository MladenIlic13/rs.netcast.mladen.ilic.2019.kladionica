package kladionicaii.kladionica.pojoDTOClasses;

import java.time.LocalDateTime;

public class NeOdigraneDTO {

	private Integer id;
	
	private String naziv;
	
	private LocalDateTime date;

	public NeOdigraneDTO() {
	}

	public NeOdigraneDTO(Integer id, String naziv, LocalDateTime date) {
		this.id = id;
		this.naziv = naziv;
		this.date = date;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "NeOdigraneDTO [id=" + id + ", naziv=" + naziv + ", date=" + date + "]";
	}


	
}
