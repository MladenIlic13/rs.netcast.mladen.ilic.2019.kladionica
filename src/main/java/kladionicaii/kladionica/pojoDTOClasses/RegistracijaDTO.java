
package kladionicaii.kladionica.pojoDTOClasses;

public class RegistracijaDTO {

	private String naziv;

	private String email;

	public RegistracijaDTO() {
	}

	public RegistracijaDTO(String naziv, String email) {
		this.naziv = naziv;
		this.email = email;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "RegistracijaDTO [id=" + ", naziv=" + naziv + ", email=" + email + "]";
	}

}
