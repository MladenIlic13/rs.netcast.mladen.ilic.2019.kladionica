
package kladionicaii.kladionica.pojoDTOClasses;
  
import java.time.LocalDateTime;

import kladionicaii.kladionica.pojoClasses.Korisnik;
  
public class NovcaneTransakcijeInputDTO {
  
	private LocalDateTime odvremena;
  
	private LocalDateTime dovremena;
  
	private Korisnik korisnik;
  
	public NovcaneTransakcijeInputDTO() {
	}

	public NovcaneTransakcijeInputDTO(LocalDateTime odvremena, LocalDateTime dovremena, Korisnik korisnik) {
		this.odvremena = odvremena;
		this.dovremena = dovremena;
		this.korisnik = korisnik;
	}

	public LocalDateTime getOdvremena() {
		return odvremena;
	}

	public void setOdvremena(LocalDateTime odvremena) {
		this.odvremena = odvremena;
	}

	public LocalDateTime getDovremena() {
		return dovremena;
	}

	public void setDovremena(LocalDateTime dovremena) {
		this.dovremena = dovremena;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	@Override
	public String toString() {
		return "NovcaneTransakcijeInputDTO [odvremena=" + odvremena + ", dovremena=" + dovremena + ", korisnik="
				+ korisnik + "]";
	}
  
}
 