
package kladionicaii.kladionica.pojoDTOClasses;
  
import java.math.BigDecimal;
import java.time.LocalDateTime;
  
public class NovcaneTransakcijeOutputDTO {
  
	private Integer idTransakcije;
	
	private String tipSvrha;
	
	private BigDecimal iznos;

	private LocalDateTime vreme;
    
	public NovcaneTransakcijeOutputDTO() {
	}

	public NovcaneTransakcijeOutputDTO(String tipSvrha, BigDecimal iznos, LocalDateTime vreme) {
		this.tipSvrha = tipSvrha;
		this.iznos = iznos;
		this.vreme = vreme;
	}

	public NovcaneTransakcijeOutputDTO(Integer idTransakcije, String tipSvrha, BigDecimal iznos, LocalDateTime vreme) {
		this.idTransakcije = idTransakcije;
		this.tipSvrha = tipSvrha;
		this.iznos = iznos;
		this.vreme = vreme;
	}

	public Integer getIdTransakcije() {
		return idTransakcije;
	}

	public void setIdTransakcije(Integer idTransakcije) {
		this.idTransakcije = idTransakcije;
	}

	public String getTipSvrha() {
		return tipSvrha;
	}

	public void setTipSvrha(String tipSvrha) {
		this.tipSvrha = tipSvrha;
	}

	public BigDecimal getIznos() {
		return iznos;
	}

	public void setIznos(BigDecimal iznos) {
		this.iznos = iznos;
	}

	public LocalDateTime getVreme() {
		return vreme;
	}

	public void setVreme(LocalDateTime vreme) {
		this.vreme = vreme;
	}

	@Override
	public String toString() {
		return "NovcaneTransakcijeOutputDTO [idTransakcije=" + idTransakcije + ", tipSvrha=" + tipSvrha + ", iznos="
				+ iznos + ", vreme=" + vreme + "]";
	}
	
}
 