package kladionicaii.kladionica.pojoDTOClasses;

import java.math.BigDecimal;

import kladionicaii.kladionica.pojoClasses.Korisnik;

public class UplataIsplataDTO {

	private BigDecimal iznos;

    private Korisnik korisnik;
	
	public UplataIsplataDTO() {
	}

	public UplataIsplataDTO(BigDecimal iznos, Korisnik korisnik) {
		this.iznos = iznos;
		this.korisnik = korisnik;
	}

	public BigDecimal getIznos() {
		return iznos;
	}

	public void setIznos(BigDecimal iznos) {
		this.iznos = iznos;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	@Override
	public String toString() {
		return "UplataIsplataDTO [id=" + ", iznos=" + iznos + ", korisnik=" + korisnik + "]";
	}

}
