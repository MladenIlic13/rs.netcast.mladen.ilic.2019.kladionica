package kladionicaii.kladionica.daoClasses;

import org.springframework.data.jpa.repository.JpaRepository;

import kladionicaii.kladionica.pojoClasses.Utakmica;

public interface UtakmicaRepository extends JpaRepository<Utakmica, Integer>{
}
