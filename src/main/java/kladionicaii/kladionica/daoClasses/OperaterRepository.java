package kladionicaii.kladionica.daoClasses;

import org.springframework.data.jpa.repository.JpaRepository;

import kladionicaii.kladionica.pojoClasses.Operater;

// custom endpoint for all data jpa ???
//@RepositoryRestResource(path="members")
public interface OperaterRepository extends JpaRepository<Operater, Integer>{
	
	// custom query?
	//public Tiket findByNaziv(String naziv);
	
}
