package kladionicaii.kladionica.daoClasses;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import kladionicaii.kladionica.pojoClasses.Tiket;

public interface TiketRepository extends JpaRepository<Tiket, Integer>{
	
	 List<Tiket> findByObradjen(Integer obradjen);

}
