package kladionicaii.kladionica.daoClasses;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import kladionicaii.kladionica.pojoClasses.Korisnik;
import kladionicaii.kladionica.pojoClasses.Racun;

public interface RacunRepository extends JpaRepository<Racun, Integer>{
	
	List<Racun> findByKorisnikAndVremeBetween(Korisnik korisnik,
			LocalDateTime odVremena, LocalDateTime doVremena);
	
}
