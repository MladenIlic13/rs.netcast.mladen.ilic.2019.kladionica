package kladionicaii.kladionica.daoClasses;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import kladionicaii.kladionica.pojoClasses.Tiket;
import kladionicaii.kladionica.pojoClasses.UtakmicaTiket;

public interface UtakmicaTiketRepository extends JpaRepository<UtakmicaTiket, Integer>{
	
	List<UtakmicaTiket> findByTiket(Tiket tiket);
	
}
