package kladionicaii.kladionica.customClasses;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import kladionicaii.kladionica.daoClasses.RacunRepository;
import kladionicaii.kladionica.daoClasses.TiketRepository;
import kladionicaii.kladionica.daoClasses.UtakmicaRepository;
import kladionicaii.kladionica.daoClasses.UtakmicaTiketRepository;
import kladionicaii.kladionica.pojoClasses.Korisnik;
import kladionicaii.kladionica.pojoClasses.Racun;
import kladionicaii.kladionica.pojoClasses.Tiket;
import kladionicaii.kladionica.pojoClasses.Utakmica;
import kladionicaii.kladionica.pojoClasses.UtakmicaTiket;

@Component
public class Scheduling {
	
	private UtakmicaRepository utakmicaRepository;
	private TiketRepository tiketRepository;
	private UtakmicaTiketRepository utakmicaTiketRepository;
	private JavaMailSender emailSender;
	private RacunRepository racunRepository;
	
	@Autowired
	public Scheduling(UtakmicaRepository utakmicaRepository, TiketRepository tiketRepository,
			UtakmicaTiketRepository utakmicaTiketRepository, JavaMailSender emailSender,
			RacunRepository racunRepository) {
		this.utakmicaRepository = utakmicaRepository;
		this.tiketRepository = tiketRepository;
		this.utakmicaTiketRepository = utakmicaTiketRepository;
		this.emailSender = emailSender;
		this.racunRepository = racunRepository;
	}
	
	private void sendmail(String text, String email) {
		SimpleMailMessage message = new SimpleMailMessage(); 
        message.setTo(email); 
        message.setSubject("resultati tiketa"); 
        message.setText(text);
        emailSender.send(message);
	}
	
	@Transactional
	public void uplataNagrade(BigDecimal vrednost, Korisnik korisnik) {
		Racun racun = new Racun("nagrada", vrednost, LocalDateTime.now(), korisnik);
		racunRepository.save(racun);
	}
	
	// setuj flag utakmicama
	@Scheduled(fixedDelay=1000)
	public void razresi() {
		
		Random random = new Random();
		List<Utakmica> utakmice = utakmicaRepository.findAll();
		
		for(Utakmica utakmica : utakmice) {
			if(utakmica.getDate().isBefore(LocalDateTime.now())) {
				if(utakmica.getFlag() == null) {
					utakmica.setFlag(random.nextInt(3));
			        utakmicaRepository.save(utakmica);
				}
			}
		}
	}

	// ostavi sistem vreme
	@Scheduled(cron=" 5 3 15 * * ?")
	public void scheduleMethod() {
		nagrada();
	}
	
	public void nagrada() {
		
		// ne obradjeni tiketi
		List<Tiket> tiketi = tiketRepository.findByObradjen(0);
		
		System.out.println();
		System.out.println("lista tiketa neobradjenih " + tiketi.toString());
		
		for(Tiket tiket : tiketi) {
			
			System.out.println();
			System.out.println("    tiket u pitanju " + tiket.toString());
			
			int velicinaTiketa = 0;
			int brojOdigranihUtakmica = 0;
			int brojPogodaka = 0;
			
			BigDecimal vrednostTiketa = tiket.getUplaceno();
			BigDecimal nagrada = vrednostTiketa;

			System.out.println("    osnovna vrednost nagrade za ovaj tiket " + nagrada);
			
			// svi tipovi vezani za ovaj tiket
			List<UtakmicaTiket> utakmicaTiketi = utakmicaTiketRepository.findByTiket(tiket);
			
			System.out.println("    utakmica-tiketi za ovaj tiket " + utakmicaTiketi.toString());
			
			for(UtakmicaTiket utakmicaTiket : utakmicaTiketi) {
				
				System.out.println("        utakmica-tiket u pitanju " + utakmicaTiket.getId());
				
				// izbroj utakmice na tiketu
				velicinaTiketa++;
				
				System.out.println("        velicina tiketa dodaj");
				
				// i ako je utakmica odigrana za taj tip
				if(utakmicaTiket.getUtakmica().getFlag() != null) {
					brojOdigranihUtakmica++;
					
					System.out.println("        broj odigranih utakmica dodaj");
					
					// i ako je pogodak
					if(utakmicaTiket.getPredvidjanje()==utakmicaTiket.getUtakmica().getFlag()) {
						brojPogodaka++;
						// izracunaj nagradu
						nagrada = nagrada.multiply(BigDecimal.valueOf(1.5));
						
						System.out.println("        broj pogodaka dodaj");
					}
				}
			}
		
			System.out.println("    velicina tiketa total " + velicinaTiketa);
			System.out.println("    broj odigranih utakmica " + brojOdigranihUtakmica);
			System.out.println("    broj pogodaka " + brojPogodaka);
			
			if(velicinaTiketa == brojOdigranihUtakmica) {
				
				// obradi utakmicu
				tiket.setObradjen(1);
				tiketRepository.save(tiket);
				
				System.out.println("    obrada tiketa");
				
				// posalji mailove
				
				if(velicinaTiketa != brojPogodaka) {
					String text = "od ukupno " + velicinaTiketa + 
							" pogadjanih utakmica imali ste " + brojPogodaka + 
							" pogodaka. Vise srece drugi put.";
						sendmail(text, tiket.getKorisnik().getEmail());
					
					System.out.println("    " + text);
					
				} else if (velicinaTiketa == brojPogodaka) {
					String text = "od ukupno " + velicinaTiketa + 
							" pogadjanih utakmica imali ste " + brojPogodaka + " pogodaka." +
							" Cestitamo osvojili ste nagradu u vrednosti " + nagrada + " eura.";
						sendmail(text, tiket.getKorisnik().getEmail());
					
					System.out.println("    " + text);
					
					// uplati nagradu
					uplataNagrade(nagrada, tiket.getKorisnik());
					
					System.out.println("    nagrada uplacena " + nagrada);
				}	
			}	
		}
		System.out.println();
	}
}
