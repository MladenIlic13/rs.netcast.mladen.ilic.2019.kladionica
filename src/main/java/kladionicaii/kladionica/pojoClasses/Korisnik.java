package kladionicaii.kladionica.pojoClasses;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="korisnik")
public class Korisnik {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idkorisnik")
	private Integer id;
	
	@Column(name="nazivkorisnik")
	private String naziv;

	@Column(name="emailkorisnik")
	private String email;
	
	public Korisnik() {
	}

	public Korisnik(String naziv, String email) {
		this.naziv = naziv;
		this.email = email;
	}

	public Korisnik(Integer id, String naziv, String email) {
		this.id = id;
		this.naziv = naziv;
		this.email = email;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Korisnik [id=" + id + ", naziv=" + naziv + ", email=" + email + "]";
	}

}
