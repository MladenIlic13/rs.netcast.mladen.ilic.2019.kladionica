package kladionicaii.kladionica.pojoClasses;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="utakmica")
public class Utakmica {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idutakmica")
	private Integer id;
	
	@Column(name="nazivutakmica")
	private String naziv;

	@Column(name="flagutakmica")
	private Integer flag;
	
	@Column(name="datumutakmica")
	private LocalDateTime date;

	@JoinColumn(name = "fkidoperater")
    @ManyToOne(optional = false)
    private Operater operater;
	
	public Utakmica() {
	}

	public Utakmica(String naziv, Integer flag, LocalDateTime date, Operater operater) {
		this.naziv = naziv;
		this.flag = flag;
		this.date = date;
		this.operater = operater;
	}

	public Utakmica(Integer id, String naziv, Integer flag, LocalDateTime date, Operater operater) {
		this.id = id;
		this.naziv = naziv;
		this.flag = flag;
		this.date = date;
		this.operater = operater;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public Operater getOperater() {
		return operater;
	}

	public void setOperater(Operater operater) {
		this.operater = operater;
	}

	@Override
	public String toString() {
		return "Utakmica [id=" + id + ", naziv=" + naziv + ", flag=" + flag + ", date=" + date + ", operater="
				+ operater + "]";
	}
	
}
