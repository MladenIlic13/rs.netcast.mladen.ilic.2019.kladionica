package kladionicaii.kladionica.pojoClasses;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="operater")
public class Operater {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idoperater")
	private Integer id;
	
	@Column(name="nazivoperater")
	private String naziv;

	public Operater() {
	}

	public Operater(String naziv) {
		this.naziv = naziv;
	}

	public Operater(Integer id, String naziv) {
		this.id = id;
		this.naziv = naziv;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	@Override
	public String toString() {
		return "Operater [id=" + id + ", naziv=" + naziv + "]";
	}
	
}
