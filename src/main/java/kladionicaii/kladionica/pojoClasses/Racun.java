package kladionicaii.kladionica.pojoClasses;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="racun")
public class Racun {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idracun")
	private Integer id;

	@Column(name="svrha")
	private String svrha;
	
	@Column(name="iznos")
	private BigDecimal iznos;
	
	@Column(name="vreme")
	private LocalDateTime vreme;

	@JoinColumn(name = "fkidkorisnik")
    @ManyToOne(optional = false)
    private Korisnik korisnik;
	
	public Racun() {
	}

	public Racun(String svrha, BigDecimal iznos, LocalDateTime vreme, Korisnik korisnik) {
		this.svrha = svrha;
		this.iznos = iznos;
		this.vreme = vreme;
		this.korisnik = korisnik;
	}

	public Racun(Integer id, String svrha, BigDecimal iznos, LocalDateTime vreme, Korisnik korisnik) {
		this.id = id;
		this.svrha = svrha;
		this.iznos = iznos;
		this.vreme = vreme;
		this.korisnik = korisnik;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSvrha() {
		return svrha;
	}

	public void setSvrha(String svrha) {
		this.svrha = svrha;
	}

	public BigDecimal getIznos() {
		return iznos;
	}

	public void setIznos(BigDecimal iznos) {
		this.iznos = iznos;
	}

	public LocalDateTime getVreme() {
		return vreme;
	}

	public void setVreme(LocalDateTime vreme) {
		this.vreme = vreme;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	@Override
	public String toString() {
		return "Racun [id=" + id + ", svrha=" + svrha + ", iznos=" + iznos + ", vreme=" + vreme + ", korisnik="
				+ korisnik + "]";
	}
	
}
