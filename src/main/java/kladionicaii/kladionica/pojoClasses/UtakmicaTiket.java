package kladionicaii.kladionica.pojoClasses;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="utakmicatiket")
public class UtakmicaTiket {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idutakmicatiket")
	private Integer id;
	
	@Column(name="predvidjanjeutakmicatiket")
	private Integer predvidjanje;

	@JoinColumn(name = "fkidtiket")  //, referencedColumnName="idtiket")
    @ManyToOne(optional = false)
    private Tiket tiket;
	
	@JoinColumn(name = "fkidutakmica")
    @ManyToOne(optional = false)
    private Utakmica utakmica;
	
	public UtakmicaTiket() {
	}

	public UtakmicaTiket(Integer predvidjanje, Utakmica utakmica) {
		this.predvidjanje = predvidjanje;
		this.utakmica = utakmica;
	}

	public UtakmicaTiket(Integer id, Integer predvidjanje, Utakmica utakmica) {
		this.id = id;
		this.predvidjanje = predvidjanje;
		this.utakmica = utakmica;
	}

	public UtakmicaTiket(Integer predvidjanje, Tiket tiket, Utakmica utakmica) {
		this.predvidjanje = predvidjanje;
		this.tiket = tiket;
		this.utakmica = utakmica;
	}

	public UtakmicaTiket(Integer id, Integer predvidjanje, Tiket tiket, Utakmica utakmica) {
		this.id = id;
		this.predvidjanje = predvidjanje;
		this.tiket = tiket;
		this.utakmica = utakmica;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPredvidjanje() {
		return predvidjanje;
	}

	public void setPredvidjanje(Integer predvidjanje) {
		this.predvidjanje = predvidjanje;
	}

	public Tiket getTiket() {
		return tiket;
	}

	public void setTiket(Tiket tiket) {
		this.tiket = tiket;
	}

	public Utakmica getUtakmica() {
		return utakmica;
	}

	public void setUtakmica(Utakmica utakmica) {
		this.utakmica = utakmica;
	}

	@Override
	public String toString() {
		return "UtakmicaTiket [id=" + id + ", predvidjanje=" + predvidjanje + ", tiket=" + tiket + ", utakmica="
				+ utakmica + "]";
	}
	
}
