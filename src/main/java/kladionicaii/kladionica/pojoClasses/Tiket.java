package kladionicaii.kladionica.pojoClasses;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tiket")
public class Tiket {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idtiket")
	private Integer id;

	@Column(name="uplaceno")
	private BigDecimal uplaceno;
	
	@Column(name="obradjen")
	private Integer obradjen;
	
	@JoinColumn(name = "fkidkorisnik")
    @ManyToOne(optional = false)
    private Korisnik korisnik;
	
	// test
	// fetch lazy is default, fetch = FetchType.LAZY, orphanRemoval = true
//	@OneToMany(cascade = CascadeType.ALL, mappedBy = "tiket")
	// @JoinColumn(name = "fkidtiket")  // needs only for unidirectional???
	// list or set
//	private List<UtakmicaTiket> utakmicaTiket;
	
	public Tiket() {
	}
	
	public Tiket(BigDecimal uplaceno, Korisnik korisnik) {
		this.uplaceno = uplaceno;
		this.korisnik = korisnik;
		this.obradjen = 0;
	}

	public Tiket(Integer id, BigDecimal uplaceno, Korisnik korisnik) {
		this.id = id;
		this.uplaceno = uplaceno;
		this.korisnik = korisnik;
		this.obradjen = 0;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public BigDecimal getUplaceno() {
		return uplaceno;
	}

	public void setUplaceno(BigDecimal uplaceno) {
		this.uplaceno = uplaceno;
	}

	public Integer getObradjen() {
		return obradjen;
	}

	public void setObradjen(Integer obradjen) {
		this.obradjen = obradjen;
	}

	@Override
	public String toString() {
		return "Tiket [id=" + id + ", uplaceno=" + uplaceno + ", obradjen=" + obradjen + ", korisnik=" + korisnik + "]";
	}

}
