
package kladionicaii.kladionica.restDTOClasses;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kladionicaii.kladionica.pojoDTOClasses.KupovinaTiketaDTO;
import kladionicaii.kladionica.pojoDTOClasses.NeOdigraneDTO;
import kladionicaii.kladionica.pojoDTOClasses.NovcaneTransakcijeInputDTO;
import kladionicaii.kladionica.pojoDTOClasses.NovcaneTransakcijeOutputDTO;
import kladionicaii.kladionica.pojoDTOClasses.RegistracijaDTO;
import kladionicaii.kladionica.pojoDTOClasses.UplataIsplataDTO;
import kladionicaii.kladionica.servicezDTOClasses.KupovinaTiketaServiceDTO;
import kladionicaii.kladionica.servicezDTOClasses.NeOdigraneServiceDTO;
import kladionicaii.kladionica.servicezDTOClasses.NovcaneTransakcijeServiceDTO;
import kladionicaii.kladionica.servicezDTOClasses.RegistracijaServiceDTO;
import kladionicaii.kladionica.servicezDTOClasses.UplataIsplataServiceDTO;

@RestController
@RequestMapping("/api")
public class RestControllerDTO {
	
	@Autowired
	private RegistracijaServiceDTO registracijaServisDTO;
	@Autowired
	private UplataIsplataServiceDTO uplataIsplataServiceDTO;
	@Autowired
	private KupovinaTiketaServiceDTO kupovinaTiketaServiceDTO;
	@Autowired
	private NeOdigraneServiceDTO neOdigraneServiceDTO;
	@Autowired
	private NovcaneTransakcijeServiceDTO novcaneTransakcijeServiceDTO;
	
	@PostMapping("/registracija")
	public RegistracijaDTO registracija(@RequestBody RegistracijaDTO registracija) {
		registracijaServisDTO.save(registracija);
		return registracija;
	}
	
	@PostMapping("/uplata")
	public UplataIsplataDTO uplata(@RequestBody UplataIsplataDTO uplata) {
		uplataIsplataServiceDTO.uplata(uplata);
		return uplata;
	}
	
	@PostMapping("/isplata")
	public UplataIsplataDTO isplata(@RequestBody UplataIsplataDTO isplata) {
		uplataIsplataServiceDTO.isplata(isplata);
		return isplata;
	}
	
	@PostMapping("/kupovina")
	public KupovinaTiketaDTO kupovina(@RequestBody KupovinaTiketaDTO kupovina) {
		kupovinaTiketaServiceDTO.kupovina(kupovina);
		return kupovina;
	}
	
	// test
	@GetMapping("/customtest")
	public KupovinaTiketaDTO customtest() {
		return kupovinaTiketaServiceDTO.customOutput();
	}
	
	@GetMapping("/neodigrane")
	public List<NeOdigraneDTO> neOdigrane() {
		return neOdigraneServiceDTO.neOdigrane();
	}
	
	@GetMapping("/pregled")
	public List<NovcaneTransakcijeOutputDTO> pregled(@RequestBody NovcaneTransakcijeInputDTO input) {
		return novcaneTransakcijeServiceDTO.pregled(input);
	}
	
	// test
	@GetMapping("/custominput")
	public NovcaneTransakcijeInputDTO custominput() {
		return novcaneTransakcijeServiceDTO.customInput();
	}
	
}
