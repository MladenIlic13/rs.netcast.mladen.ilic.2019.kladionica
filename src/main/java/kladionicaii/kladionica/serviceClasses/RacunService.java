package kladionicaii.kladionica.serviceClasses;

import java.util.List;

import kladionicaii.kladionica.pojoClasses.Racun;

public interface RacunService {
	
	public List<Racun> findAll();
	  
	public Racun findById(Integer id);

	public Racun save(Racun racunKorisnik);

	public Racun update(Racun racunKorisnik);

	public void deleteById(Integer id);
	
	public void deleteByObject(Racun racunKorisnik);

}
