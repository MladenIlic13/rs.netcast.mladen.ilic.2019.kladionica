package kladionicaii.kladionica.serviceClasses;

import java.util.List;

import kladionicaii.kladionica.pojoClasses.UtakmicaTiket;

public interface UtakmicaTiketService {
	
	public List<UtakmicaTiket> findAll();
	  
	public UtakmicaTiket findById(Integer id);

	public UtakmicaTiket save(UtakmicaTiket utakmicaTiket);

	public UtakmicaTiket update(UtakmicaTiket utakmicaTiket);

	public void deleteById(Integer id);
	
	public void deleteByObject(UtakmicaTiket utakmicaTiket);

}
