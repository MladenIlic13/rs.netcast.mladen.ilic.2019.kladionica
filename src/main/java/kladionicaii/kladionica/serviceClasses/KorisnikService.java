package kladionicaii.kladionica.serviceClasses;

import java.util.List;

import kladionicaii.kladionica.pojoClasses.Korisnik;

public interface KorisnikService {
	
	public List<Korisnik> findAll();
	  
	public Korisnik findById(Integer id);

	public Korisnik save(Korisnik korisnik);
	
	public Korisnik update(Korisnik korisnik);

	public void deleteById(Integer id);
	
	public void deleteByObject(Korisnik korisnik);

}
