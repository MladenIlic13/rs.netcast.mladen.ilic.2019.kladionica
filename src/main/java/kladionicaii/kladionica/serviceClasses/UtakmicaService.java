package kladionicaii.kladionica.serviceClasses;

import java.util.List;

import kladionicaii.kladionica.pojoClasses.Utakmica;

public interface UtakmicaService {
	
	public List<Utakmica> findAll();
	  
	public Utakmica findById(Integer id);

	public Utakmica save(Utakmica utakmica);

	public Utakmica update(Utakmica utakmica);

	public void deleteById(Integer id);
	
	public void deleteByObject(Utakmica utakmica);

}
