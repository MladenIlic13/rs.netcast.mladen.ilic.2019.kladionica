package kladionicaii.kladionica.serviceClasses;

import java.util.List;

import kladionicaii.kladionica.pojoClasses.Tiket;

public interface TiketService {
	
	public List<Tiket> findAll();
	  
	public Tiket findById(Integer id);

	public Tiket save(Tiket tiket);

	public Tiket update(Tiket tiket);

	public void deleteById(Integer id);
	
	public void deleteByObject(Tiket tiket);

}
